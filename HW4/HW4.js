// Завдання
// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.



const firstNumber = +prompt('Enter first number');
const secondNumber = +prompt('Enter second number');
const operator = prompt('Enter operand')

function calculator(a, b, c) {
    switch (c) {
        case '+' :
            return a + b;
        case '*' :
                return a * b;
        case '-' :
            return a - b;
        case '/' :
            return a / b;
    }
}

console.log(calculator(firstNumber, secondNumber, operator));

