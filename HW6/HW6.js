// Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) 
// і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) 
// і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому 
// регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. 
// (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// // - Вивести в консоль результат роботи функції `createNewUser()`, 
// а також функцій `getAge()` та `getPassword()` створеного об'єкта.


function createNewUser() {
    let firstName = prompt('Enter you name');
    let lastName = prompt('Enter you surname');
    let birthday = prompt("Enter your birthday date", "dd.mm.yyyy");
    let date = new Date;
    let birthdayMiliseconds = Date.parse(birthday.slice(6) + '.' + birthday.slice(3, 6) + birthday.slice(0, 2));

    let newUser = {
        firstName,
        lastName,
        birthday,
        birthdayMiliseconds,
        date,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge() {
            return Math.floor((this.date - this.birthdayMiliseconds) / (1000 * 3600 * 24 * 365))
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + new Date (this.birthdayMiliseconds).getFullYear()
        },
    }
    return newUser;
}

let createUser = createNewUser();
let login = createUser.getLogin();
console.log(login);
let age = createUser.getAge();
console.log(age);
let pass = createUser.getPassword();
console.log(pass);





